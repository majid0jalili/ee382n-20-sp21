# Lab 3 — Programming with Regent #

This lab graciously provided by Alex Aiken and the Legion/Regent team at Stanford; originally developed for Alex’s course. Do not share material from this lab outside of class.
Overview
In this assignment, you will write a parallel PageRank algorithm in Regent, the basis of Google’s ranking of web pages in search results and explore a multi-GPU architecture with Regent. While PageRank is simple and straightforward, parallelizing the algorithm will require you to reason about not only the algorithm itself but also the underlying graph structure (webgraph) being processed. 

### All 3 Parts Due Date (no extension): Saturday April 11th at 23:59pm  ###

In addition to CUDA programming in Lab2, you will learn about an alternative approach: task-based parallel programming in Regent. In Part 1, you will set up Regent programming language on TACC and learn about Regent by walking through a tutorial. You will write a serial PageRank in Regent in Part 2 and will then parallelize it in the context of the Regent programming language in Part 3.

**** Again, you are advised to start early and to read all the instructions carefully before diving into them!

 

 
